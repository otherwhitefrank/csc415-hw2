/*  Name: Frank Dye
Date: 2/10/2014
Description: A basic copy program using POSIX system calls
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define BUFFER_SIZE 512
#define STRING_SIZE 128

//Open source file as read only, 
#define SOURCE_FLAGS O_RDONLY

//Open destination file as write only 
//open() fails when O_EXCL and O_CREAT are specified and the file already exists. I enable
//these flags to prevent overwriting an existing file.
#define DEST_FLAGS (O_WRONLY | O_EXCL |  O_CREAT)

//Forward declarations
int copyFile(int  fdSourceFile, int  fdDestFile);
void processError(int currentError);

int main(int argc, char** argv)
{
	//Temporary buffers for text manipulation
	char szSuccess[] = " bytes copied.\n";

	char* ptrCWD = NULL; 

	char szBuffer[STRING_SIZE];

	int cwdLength = 0;
	int bufferLength = 0;

	//Source and Dest file descriptors
	int fdSourceFile = -1;
	int fdDestFile = -1;

	//Counter for total bytes written
	int totalBytesWritten = 0;

	//File Stat, used to get source file, file permissions
	struct stat sourceFileStatistics;

	//Buffers for source and dest files to be used with sprintf
	char szSourceFileName[STRING_SIZE] = "";
	char szDestFileName[STRING_SIZE] = "";

	//Check to make sure we have the correct amount of parameters

	if (argc >= 3)
	{
		//Acquire the current working directory, getcwd should malloc a string for the path
		//when the pointer to buff is NULL and size == 0
		ptrCWD = getcwd(NULL, 0);

		//Error check getcwd, ptrCWD will == NULL, then check errno for error.
		if (ptrCWD == NULL)
		{
		  processError(errno);
		}

		//Grab the first two parameters from out command line
		sprintf(szSourceFileName, "%s/%s", ptrCWD, argv[1]);
		sprintf(szDestFileName, "%s/%s", ptrCWD, argv[2]);


		//Open source file
		fdSourceFile = open(szSourceFileName, SOURCE_FLAGS);
		

		//Check for error opening sourceFileName
		if (fdSourceFile == -1)
		{
			processError(errno);
		}


		//Get source files permissions
		if (fstat(fdSourceFile, &sourceFileStatistics) == -1)
		{
		  processError(errno);
		}


		//Open destination file
		fdDestFile = open(szDestFileName, DEST_FLAGS, sourceFileStatistics.st_mode);

		//Check for error opening destFileName
		if (fdDestFile == -1)
		{
			processError(errno);
		}

		//Files opened okay, initiate copy
		totalBytesWritten = copyFile(fdSourceFile, fdDestFile);

		//Build our success string buffer
		sprintf(szBuffer, "%d%s", totalBytesWritten, szSuccess);

		//Output number of bytes copied
		printf("%s", szBuffer);

		//Close file handles
		if (close(fdSourceFile) == -1)
		{
			processError(errno);
		}

		if (close(fdDestFile) == -1)
		{
			processError(errno);
		}

		//exit program with error code 0
		return 0;
	}
	else
	{
		printf("Incorrect number of parameters!\n");
		exit(1);
	}
}

void processError(int currentError)
{
  //strerror() prints out a char* message that corresponds to the error 
  printf("%s", strerror(currentError));
  exit(1);
}

//Helper function to loop through an already opened source file copying it to a readfile.
int copyFile(int  fdSourceFile, int  fdDestFile)
{
	//Counters for reading and writing.
	int numBytesWritten = 0;
	int numBytesRead = 0;
	int totalBytesWritten = 0;


	//Buffer to copy data into.
	char copyBuffer[BUFFER_SIZE];

	do 
	{
		//Read 512 bytes from the file
		numBytesRead = read(fdSourceFile, &copyBuffer, BUFFER_SIZE);	

		//Check for error on read()
		if (numBytesRead == -1)
		{
			processError(errno);
		}

		//We filled our buffer so try to write it out.

		numBytesWritten = write(fdDestFile, copyBuffer, numBytesRead);

		if (numBytesWritten != numBytesRead || numBytesWritten == -1)
		{
			processError(errno);
		}

		totalBytesWritten += numBytesWritten; //update counter

	} while (numBytesRead != 0);

	//Copy successful return numBytesWritten
	return totalBytesWritten;
}


/*  Name: Frank Dye
Date: 2/25/2014
Description: A basic copy program using win32 system calls
*/

#include <windows.h>
#include <stdio.h>


#define BUFFER_SIZE 512
#define STRING_SIZE 128


//Forward declarations
int copyFile(HANDLE  sourceFile, HANDLE  destFile);
void closeFileHandles(HANDLE sourceFile, HANDLE destFile);
void processError(HRESULT errorCode);


int main(int argc, char** argv)
{
	char szSuccess[] = " bytes copied.\n";
	char szCWD[BUFFER_SIZE];
	char szBuffer[STRING_SIZE];

	int cwdLength = 0;
	int bufferLength = 0;

	//Source and Dest File Handles
	HANDLE sourceFile = NULL;
	HANDLE destFile = NULL;

	//Error Code 
	HRESULT errorCode = 0;


	//Counter for total bytes written
	DWORD totalBytesWritten = 0;

	//Buffers for source and dest files to be used with sprintf
	char szSourceFileName[STRING_SIZE] = "";
	char szDestFileName[STRING_SIZE] = "";

	//Check to make sure we have the correct amount of parameters

	if (argc == 3)
	{
		
		//Acquire the current working directory
		cwdLength = GetCurrentDirectory(BUFFER_SIZE, szCWD);

		//Grab the first two parameters from out command line
		sprintf(szSourceFileName, "%s\\%s", szCWD, argv[1]);
		sprintf(szDestFileName, "%s\\%s", szCWD, argv[2]);

		//Open source file
		sourceFile = CreateFile(szSourceFileName, GENERIC_READ, 0, 
			NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
			NULL);

		//Check to see if file was opened
		errorCode = GetLastError();

		processError(errorCode); //This will exit and display message if opening file failed
		//otherwise continue processing.

		//Open destination file
		destFile = CreateFile(szDestFileName, GENERIC_WRITE, 0,
			NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL,
			NULL);

		//Check to see if file was created
		errorCode = GetLastError();

		processError(errorCode); //exit if failed, otherwise continue.

		//Files opened okay, initiate copy
		totalBytesWritten = copyFile(sourceFile, destFile);


		//Build our success string buffer
		sprintf(szBuffer, "%d%s", totalBytesWritten, szSuccess);

		//Output number of bytes copied
		printf(szBuffer);

		//Exit program with error code 0
		return 0;
	}
	else
	{
		printf("Incorrect number of parameters!\n");
		exit(1);
	}


}

//Helper function to loop through an already opened source file copying it to a readfile.
int copyFile(HANDLE  sourceFile, HANDLE  destFile)
{
	//Counters for reading and writing.
	DWORD numBytesWritten = 0;
	DWORD numBytesRead = 0;
	DWORD totalBytesWritten = 0;

	boolean operationStatus = FALSE;

	HRESULT errorCode = 0;

	//Buffer to copy data into.
	char copyBuffer[512];

	do 
	{
		//Read 512 bytes from the file
		operationStatus = ReadFile(sourceFile, &copyBuffer, 512, &numBytesRead, NULL);		

		if (operationStatus && numBytesRead != 0)
		{
			//We filled our buffer so try to write it out.
			operationStatus = WriteFile(destFile, &copyBuffer, numBytesRead, &numBytesWritten, NULL);
			totalBytesWritten += numBytesWritten; //update counter
		}

		if (!operationStatus || numBytesRead != numBytesWritten)
		{
			//Read failed so process the error
			errorCode = GetLastError();

			processError(errorCode);
		}

		//Check for EOF
		if (operationStatus && numBytesRead == 0) 
		{
			//Set operationStatus to false
			operationStatus = FALSE;
		}

	} while (operationStatus);


	//Close files
	closeFileHandles(sourceFile, destFile);

	//Copy successful return numBytesWritten
	return totalBytesWritten;
}

void closeFileHandles(HANDLE sourceFile, HANDLE destFile)
{
	//Check to see if handle is valid and if so then close it.

	//Close the handles so we dont get dangling file pointers
	if (sourceFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(sourceFile);
	}

	if (destFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(destFile);
	}
}



//Helper function to process error codes 
void processError(HRESULT errorCode)
{

	if (errorCode == ERROR_FILE_NOT_FOUND)
	{
		printf("Source File Not Found!\n");
		exit(1);
	}
	else if (errorCode == ERROR_FILE_EXISTS)
	{
		printf("Destination File Already Exists!\n");
		exit(1);
	}
	else if (errorCode == ERROR_IO_PENDING)
	{
		printf("IO Error reading/writing!");
		exit(1);
	}
	else if (errorCode == ERROR_SUCCESS)
	{
		//No error code, ignore
	}
	else if (errorCode == S_OK)
	{
		//No error, file opened okay, ignore
	}
	else
	{
		printf("Could not open files!");
		exit(1);
	}
}